class Post < ActiveRecord::Base
	has_many :comments, dependent: :destroy
	validates_presence_of :title
	validates_length_of :body, :in => 10..400, :message =>"Longitud no válida"
end
